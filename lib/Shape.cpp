#include "Shape.h"

IntersectionPoint Sphere::intersects(const Ray &R) const {
	Vector O = R.Origin - Position;
	double c = O.dot(O) - std::pow(Radius, 2);
	double b = 2 * (O.dot(R.Direction));
	double a = R.Direction.dot(R.Direction);
	double disc = b*b - 4 * a * c;

	if(disc < 0) return { nullptr, 0 };

	double t0 = (-b - sqrtf(disc)) / (2 * a);
	double t1 = (-b + sqrtf(disc)) / (2 * a);

	if(t0 > t1) std::swap(t0, t1);
	if(t1 < 0) return { nullptr, 0 };
	if(t0 < 0) return { this, t1 };
	return { this, t0 };
}

Vector Sphere::computeNormal(const Vector &P) const {
	return (P - Position).unit();
}

IntersectionPoint UnitSphere::intersects(const Ray &R) const {
	return Sphere::intersects(R.transform(Transformation.inverse()));
}

IntersectionPoint Plane::intersects(const Ray &R) const {
	double temp2 = -Normal.dot(R.Origin - Position);
	double temp3 = Normal.dot(R.Direction);
	if(temp3 == 0) return { nullptr, 0 };
	double temp4 = temp2/temp3;
	if(temp4 < 0) return { nullptr, 0 };
	return { this, temp4 };
}

IntersectionPoint UnitPlane::intersects(const Ray &R) const {
	return Plane::intersects(R.transform(Transformation.inverse()));
}

Vector Plane::computeColour(const Vector &P) const {
	if(Normal.z == -1) return {0.0, 0.9, 0.0};
	int x = floor(P.x);
	int z = floor(P.z);
	if((x ^ z) & 1) {
		return {0.2,0.2,0.2};
	} else {
		return {0.7,0.7,0.7};
	}
}

Vector Plane::computeNormal(const Vector &P) const {
	return Normal;
}

IntersectionPoint BoundedPlane::intersects(const Ray &R) const {
	IntersectionPoint IP = Plane::intersects(R);
	if(!IP.Intersection) return IP;

	Vector POI = R.Origin + R.Direction * IP.Distance;

	if(Normal.z == -1) {
		if(fabs(POI.x - Position.x) < Size && fabs(POI.y - Position.y) < Size) {
			return IP;
		}
	}

	if(Normal.y == 1) {
		if(fabs(POI.x - Position.x) < Size && fabs(POI.z - Position.z) < Size) {
			return IP;
		}
	}
	return { nullptr, 0 };
}
