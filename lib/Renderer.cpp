#include "Renderer.h"
#include "ImagePlane.h"

IntersectionPoint Renderer::getClosestIntersection(const Ray &R) {
	IntersectionPoint IP { nullptr, 10000 };
	for(Shape *S : S.Shapes) {
		IntersectionPoint IP2 = S->intersects(R);
		if(IP2.Intersection && IP2.Distance < IP.Distance) {
			IP = IP2;
		}
	}
	return IP;
}

IntersectionPoint Renderer::getAnyIntersection(const Ray &R, const Shape *Skip, double MaxDistance) {
	for(Shape *S : S.Shapes) {
		if(S == Skip) continue;
		IntersectionPoint IP2 = S->intersects(R);
		if(IP2.Intersection && IP2.Distance < MaxDistance) {
			return IP2;
		}
	}
	return { nullptr, 0 };
}

ImagePlane Renderer::render() {
	ImagePlane I(S.Width, S.Height);

	double sx = S.Width / 2.0;
	double sy = S.Height / 2.0;
	double scale = 2.0 / S.Height;

	for(int py = 0; py < S.Width; ++py) {
		for(int px = 0; px < S.Height; ++px) {
			double ny = scale * (py - sy);
			double nx = scale * (px - sx);

			Vector ImgPoint = { nx, -ny, 1 };
			Ray View = {{ 0, 0, -50 }, ImgPoint.unit()};

			IntersectionPoint IP = getClosestIntersection(View);

			Vector Colour = { 0, 0, 0 };

			if(IP.Intersection) {
				Ray TR = View.transform(IP.Intersection->Transformation.inverse());
				Vector PointOfIntersection = TR.Origin + TR.Direction * IP.Distance;
				Vector WPointOfIntersection = IP.Intersection->Transformation * PointOfIntersection;
				Vector NormalAtIntersection = IP.Intersection->Transformation * (PointOfIntersection + IP.Intersection->computeNormal(PointOfIntersection));
				NormalAtIntersection = (NormalAtIntersection - WPointOfIntersection).unit();
				Vector SurfaceColour = IP.Intersection->computeColour(PointOfIntersection);

				for(Light *L : S.Lights) {
					Vector LightPosition = L->Position - WPointOfIntersection;
					Ray ToLight = { WPointOfIntersection, LightPosition.unit() };

					IntersectionPoint LIP = getAnyIntersection(ToLight, IP.Intersection, LightPosition.length());
					if(!LIP.Intersection) {
						Vector Lambert = (SurfaceColour * L->Intensity) * (LightPosition.unit().dot(NormalAtIntersection));
						Colour = Colour + Lambert;
					}
				}
			}
			
			I(px, py) = Colour;
		}
	}
	return I;
}
