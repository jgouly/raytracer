#include <fstream>

#include "ImagePlane.h"

Vector& ImagePlane::operator()(int x, int y) {
	return Pixels[(x) + (y * Height)];
}

namespace {
	// stolen from smallpt
	double clamp(double d) {
		return d < 0 ? 0 : d > 1 ? 1 : d;
	}

	int toInt(double d) {
		return int(std::pow(clamp(d), 1/2.2)*255+0.5);
	}
};

void ImagePlane::writeToPPM(const std::string& Name) {
	std::ofstream out(Name);
	out << "P3\n" << Width << " " << Height;
	out << "\n255\n";       
	for(Vector &V : Pixels) {
		out << toInt(V.x) << " ";
		out << toInt(V.y) << " ";
		out << toInt(V.z) << " ";
	}
}
