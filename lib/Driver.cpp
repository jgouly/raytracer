#include "Renderer.h"
#include "Transformation.h"

int main() {
	Scene S = {500, 500};

	UnitPlane UP({0.5,0.5,0.5}, {0,1,0});
	UP.Transformation = Transformation().translate({0, -20, 0}).scale(1).rotateY(15);

	S.add(&UP);
	
	Light L = {{0, 10, -5}, {0.4,0.4,0.4}};
	S.add(&L);

	Renderer R = {S};
	ImagePlane I = R.render();
	I.writeToPPM();

	return 0;
}
