CXX=clang++
CXXFLAGS=-std=c++11 -O3 -I./include

driver: lib/Driver.o lib/ImagePlane.o lib/Renderer.o lib/Shape.o
	$(CXX) $^

debug: CXXFLAGS+= -O0 -g
debug: driver

clean:
	rm lib/*.o
