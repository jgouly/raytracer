#ifndef SHAPE_H
#define SHAPE_H

#include "Ray.h"
#include "Transformation.h"

struct Shape;

struct IntersectionPoint {
	const Shape *Intersection;
	double Distance;
};

struct Shape {
	Shape(Vector P, Vector C) : Position(P), Colour(C) {}

	virtual IntersectionPoint intersects(const Ray &R) const = 0;
	virtual Vector computeColour(const Vector &P) const {
		return Colour;
	}
	virtual Vector computeNormal(const Vector &P) const = 0;

	Vector Position, Colour;
	Matrix Transformation;
};

struct Sphere : Shape {
	Sphere(Vector P, double R, Vector C) : Shape(P, C), Radius(R) {}

	virtual IntersectionPoint intersects(const Ray &R) const override;
	virtual Vector computeNormal(const Vector &P) const override;

	double Radius;
};

struct UnitSphere : Sphere {
	UnitSphere(Vector C) : Sphere(Vector({ 0, 0, 0 }), 1, C) {}

	virtual IntersectionPoint intersects(const Ray &R) const override;
};

struct Plane : Shape {
	Plane(Vector P, Vector C, Vector N) : Shape(P, C), Normal(N) {}

	virtual IntersectionPoint intersects(const Ray &R) const override;
	virtual Vector computeColour(const Vector &P) const override;
	virtual Vector computeNormal(const Vector &P) const override;

	Vector Normal;
};

struct UnitPlane : Plane {
	UnitPlane(Vector C, Vector N) : Plane(Vector({0, 0, 0}), C, N) {}

	virtual IntersectionPoint intersects(const Ray &R) const override;
};

struct BoundedPlane : Plane {
	BoundedPlane(Vector P, Vector C, Vector N, double Size) : Plane(P, C, N), Size(Size) {}

	virtual IntersectionPoint intersects(const Ray &R) const override;

	double Size;
};

struct Light {
	Vector Position, Intensity;
};

#endif
