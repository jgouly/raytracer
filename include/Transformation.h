#ifndef TRANSFORMATION_H
#define TRANSFORMATION_H

#include <cmath>

#include "Matrix.h"
#include "Vector.h"

struct Transformation {
	Transformation() : M(MatrixId) {}

	operator Matrix() { return M; }

	Transformation scale(double D) {
		M = M * Matrix({ D, 0, 0, 0,
			0, D, 0, 0,
			0, 0, D, 0,
			0, 0, 0, 1.0/D });
		return *this;
	}

	Transformation translate(const Vector &V) {
		M = M * Matrix({ 1, 0, 0, V.x,
			0, 1, 0, V.y,
			0, 0, 1, V.z,
			0, 0, 0, 1 });
		return *this;
	}

	Transformation rotateX(double angle) {
		M = M * Matrix({ 1, 0, 0, 0,
			0, cos(angle), -sin(angle), 0,
			0, sin(angle), cos(angle), 0,
			0, 0, 0, 1 });
		return *this;
	}

	Transformation rotateY(double angle) {
		M = M * Matrix({ cos(angle), 0, sin(angle), 0,
			0, 1, 0, 0,
			-sin(angle), 0, cos(angle), 0,
			0, 0, 0, 1 });
		return *this;
	}

	Matrix M;
};

#endif
