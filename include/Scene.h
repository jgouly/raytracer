#ifndef SCENE_H
#define SCENE_H

#include <vector>

#include "Shape.h"

struct Scene {
	int Width, Height;
	std::vector<Shape*> Shapes;
	std::vector<Light*> Lights;

	void add(Shape* S) {
		Shapes.push_back(S);
	}

	void add(Light *L) {
		Lights.push_back(L);
	}
};

#endif
