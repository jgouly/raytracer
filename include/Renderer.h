#ifndef RENDERER_H
#define RENDERER_H

#include "Shape.h"
#include "Scene.h"
#include "ImagePlane.h"

struct Renderer {
	Renderer(const Scene &S) : S(S) {}
	const Scene &S;

	IntersectionPoint getClosestIntersection(const Ray &R);
	IntersectionPoint getAnyIntersection(const Ray &R, const Shape *Skip, double MaxDistance);
	ImagePlane render();
};

#endif
