#ifndef MATRIX_H
#define MATRIX_H

#include <array>
#include "Vector.h"

struct Matrix {
	Matrix operator+(const Matrix &M) const {
		Matrix Temp;
		
		for(int i = 0; i < 4; ++i) {
			for(int j = 0; j < 4; ++j) {
				Temp.Rows[i][j] = Rows[i][j] + M.Rows[i][j];
			}
		}

		return Temp;
	}

	Matrix operator-(const Matrix &M) const {
		Matrix Temp;
		
		for(int i = 0; i < 4; ++i) {
			for(int j = 0; j < 4; ++j) {
				Temp.Rows[i][j] = Rows[i][j] - M.Rows[i][j];
			}
		}

		return Temp;
	}

	Matrix operator*(double D) const {
		Matrix Temp;
		
		for(int i = 0; i < 4; ++i) {
			for(int j = 0; j < 4; ++j) {
				Temp.Rows[i][j] = Rows[i][j] * D;
			}
		}

		return Temp;
	}

	Matrix operator*(const Matrix &M) const {
		Matrix Temp;
		
		for(int i = 0; i < 4; ++i) {
			for(int j = 0; j < 4; ++j) {
				double sum = 0;
				for(int k = 0; k < 4; ++k) {
					sum += Rows[i][k] * M.Rows[k][j];
				}
				Temp.Rows[i][j] = sum;
			}
		}

		return Temp;
	}

	Vector operator*(const Vector &V) const {
		double x0 = V.x * Rows[0][0] +
			V.y * Rows[0][1] +
			V.z * Rows[0][2] +
			Rows[0][3];
		double y0 = V.x * Rows[1][0] +
			V.y * Rows[1][1] +
			V.z * Rows[1][2] +
			Rows[1][3];
		double z0 = V.x * Rows[2][0] +
			V.y * Rows[2][1] +
			V.z * Rows[2][2] +
			Rows[2][3];
		return { x0, y0, z0 };
	}

	double det() const {
		return Rows[0][0]*Rows[1][1]*Rows[2][2]*Rows[3][3] -
					Rows[0][0]*Rows[1][2]*Rows[2][1]*Rows[3][3] +
					Rows[0][0]*Rows[1][2]*Rows[2][3]*Rows[3][1] -
					Rows[0][0]*Rows[1][3]*Rows[2][2]*Rows[3][1] -
					Rows[0][1]*Rows[1][2]*Rows[2][3]*Rows[3][0] +
					Rows[0][1]*Rows[1][3]*Rows[2][2]*Rows[3][0] +
					Rows[0][2]*Rows[1][1]*Rows[2][3]*Rows[3][0] -
					Rows[0][2]*Rows[1][3]*Rows[2][1]*Rows[3][0] -
					Rows[0][0]*Rows[1][1]*Rows[2][3]*Rows[3][2] +
					Rows[0][0]*Rows[1][3]*Rows[2][1]*Rows[3][2] -
					Rows[0][1]*Rows[1][0]*Rows[2][2]*Rows[3][3] +
					Rows[0][1]*Rows[1][2]*Rows[2][0]*Rows[3][3] +
					Rows[0][2]*Rows[1][0]*Rows[2][1]*Rows[3][3] -
					Rows[0][2]*Rows[1][0]*Rows[2][3]*Rows[3][1] -
					Rows[0][2]*Rows[1][1]*Rows[2][0]*Rows[3][3] +
					Rows[0][2]*Rows[1][3]*Rows[2][0]*Rows[3][1] -
					Rows[0][3]*Rows[1][1]*Rows[2][2]*Rows[3][0] +
					Rows[0][3]*Rows[1][2]*Rows[2][1]*Rows[3][0] +
					Rows[0][1]*Rows[1][0]*Rows[2][3]*Rows[3][2] -
					Rows[0][1]*Rows[1][3]*Rows[2][0]*Rows[3][2] +
					Rows[0][3]*Rows[1][0]*Rows[2][2]*Rows[3][1] -
					Rows[0][3]*Rows[1][2]*Rows[2][0]*Rows[3][1] -
					Rows[0][3]*Rows[1][0]*Rows[2][1]*Rows[3][2] +
					Rows[0][3]*Rows[1][1]*Rows[2][0]*Rows[3][2];
	}

	Matrix inverse() const {
		Matrix Temp;
		double Det = det();
		Temp.Rows[0][0] = (Rows[1][1]*Rows[2][2]*Rows[3][3] - Rows[1][2]*Rows[2][1]*
			Rows[3][3] + Rows[1][2]*Rows[2][3]*Rows[3][1] - Rows[1][3]*Rows[2][2]*
			Rows[3][1] - Rows[1][1]*Rows[2][3]*Rows[3][2] + Rows[1][3]*Rows[2][1]*Rows[3][2])/Det;
		Temp.Rows[0][1] = -(Rows[0][1]*Rows[2][2]*Rows[3][3] - Rows[0][2]*Rows[2][1]*
			Rows[3][3] + Rows[0][2]*Rows[2][3]*Rows[3][1] - Rows[0][1]*Rows[2][3]*Rows[3][2] -
			Rows[0][3]*Rows[2][2]*Rows[3][1] + Rows[0][3]*Rows[2][1]*Rows[3][2])/Det;
		Temp.Rows[0][2] = (Rows[0][1]*Rows[1][2]*Rows[3][3] - Rows[0][2]*Rows[1][1]*
			Rows[3][3] + Rows[0][2]*Rows[1][3]*Rows[3][1] - Rows[0][1]*Rows[1][3]*Rows[3][2] -
			Rows[0][3]*Rows[1][2]*Rows[3][1] + Rows[0][3]*Rows[1][1]*Rows[3][2])/Det;
		Temp.Rows[0][3] = -(Rows[0][1]*Rows[1][2]*Rows[2][3] - Rows[0][1]*Rows[1][3]*
			Rows[2][2] - Rows[0][2]*Rows[1][1]*Rows[2][3] + Rows[0][2]*Rows[1][3]*Rows[2][1] +
			Rows[0][3]*Rows[1][1]*Rows[2][2] - Rows[0][3]*Rows[1][2]*Rows[2][1])/Det;
		Temp.Rows[1][0] = -(Rows[1][2]*Rows[2][3]*Rows[3][0] - Rows[1][3]*Rows[2][2]*
			Rows[3][0] + Rows[1][0]*Rows[2][2]*Rows[3][3] - Rows[1][2]*Rows[2][0]*Rows[3][3] -
			Rows[1][0]*Rows[2][3]*Rows[3][2] + Rows[1][3]*Rows[2][0]*Rows[3][2])/Det;
		Temp.Rows[1][1] = (Rows[0][0]*Rows[2][2]*Rows[3][3] + Rows[0][2]*Rows[2][3]*
			Rows[3][0] - Rows[0][0]*Rows[2][3]*Rows[3][2] - Rows[0][2]*Rows[2][0]*Rows[3][3] -
			Rows[0][3]*Rows[2][2]*Rows[3][0] + Rows[0][3]*Rows[2][0]*Rows[3][2])/Det;
		Temp.Rows[1][2] = -(Rows[0][0]*Rows[1][2]*Rows[3][3] + Rows[0][2]*Rows[1][3]*
			Rows[3][0] - Rows[0][0]*Rows[1][3]*Rows[3][2] - Rows[0][2]*Rows[1][0]*Rows[3][3] -
			Rows[0][3]*Rows[1][2]*Rows[3][0] + Rows[0][3]*Rows[1][0]*Rows[3][2])/Det;
		Temp.Rows[1][3] = (Rows[0][0]*Rows[1][2]*Rows[2][3] - Rows[0][0]*Rows[1][3]*
			Rows[2][2] - Rows[0][2]*Rows[1][0]*Rows[2][3] + Rows[0][2]*Rows[1][3]*Rows[2][0] +
			Rows[0][3]*Rows[1][0]*Rows[2][2] - Rows[0][3]*Rows[1][2]*Rows[2][0])/Det;
		Temp.Rows[2][0] = (Rows[1][1]*Rows[2][3]*Rows[3][0] - Rows[1][3]*Rows[2][1]*
			Rows[3][0] + Rows[1][0]*Rows[2][1]*Rows[3][3] - Rows[1][0]*Rows[2][3]*Rows[3][1] -
			Rows[1][1]*Rows[2][0]*Rows[3][3] + Rows[1][3]*Rows[2][0]*Rows[3][1])/Det;
		Temp.Rows[2][1] = -(Rows[0][0]*Rows[2][1]*Rows[3][3] - Rows[0][0]*Rows[2][3]*
			Rows[3][1] + Rows[0][1]*Rows[2][3]*Rows[3][0] - Rows[0][1]*Rows[2][0]*Rows[3][3] -
			Rows[0][3]*Rows[2][1]*Rows[3][0] + Rows[0][3]*Rows[2][0]*Rows[3][1])/Det;
		Temp.Rows[2][2] = (Rows[0][0]*Rows[1][1]*Rows[3][3] - Rows[0][0]*Rows[1][3]*
			Rows[3][1] + Rows[0][1]*Rows[1][3]*Rows[3][0] - Rows[0][1]*Rows[1][0]*Rows[3][3] -
			Rows[0][3]*Rows[1][1]*Rows[3][0] + Rows[0][3]*Rows[1][0]*Rows[3][1])/Det;
		Temp.Rows[2][3] = -(Rows[0][0]*Rows[1][1]*Rows[2][3] - Rows[0][0]*Rows[1][3]*
			Rows[2][1] - Rows[0][1]*Rows[1][0]*Rows[2][3] + Rows[0][1]*Rows[1][3]*Rows[2][0] +
			Rows[0][3]*Rows[1][0]*Rows[2][1] - Rows[0][3]*Rows[1][1]*Rows[2][0])/Det;
		Temp.Rows[3][0] = -(Rows[1][1]*Rows[2][2]*Rows[3][0] - Rows[1][2]*Rows[2][1]*
			Rows[3][0] - Rows[1][0]*Rows[2][2]*Rows[3][1] + Rows[1][2]*Rows[2][0]*Rows[3][1] +
			Rows[1][0]*Rows[2][1]*Rows[3][2] - Rows[1][1]*Rows[2][0]*Rows[3][2])/Det;
		Temp.Rows[3][1] = -(Rows[0][0]*Rows[2][2]*Rows[3][1] - Rows[0][1]*Rows[2][2]*
			Rows[3][0] + Rows[0][2]*Rows[2][1]*Rows[3][0] - Rows[0][0]*Rows[2][1]*Rows[3][2] -
			Rows[0][2]*Rows[2][0]*Rows[3][1] + Rows[0][1]*Rows[2][0]*Rows[3][2])/Det;
		Temp.Rows[3][2] = (Rows[0][0]*Rows[1][2]*Rows[3][1] - Rows[0][1]*Rows[1][2]*
			Rows[3][0] + Rows[0][2]*Rows[1][1]*Rows[3][0] - Rows[0][0]*Rows[1][1]*Rows[3][2] -
			Rows[0][2]*Rows[1][0]*Rows[3][1] + Rows[0][1]*Rows[1][0]*Rows[3][2])/Det;
		Temp.Rows[3][3] = (Rows[0][0]*Rows[1][1]*Rows[2][2] - Rows[0][0]*Rows[1][2]*
			Rows[2][1] - Rows[0][1]*Rows[1][0]*Rows[2][2] + Rows[0][1]*Rows[1][2]*Rows[2][0] +
			Rows[0][2]*Rows[1][0]*Rows[2][1] - Rows[0][2]*Rows[1][1]*Rows[2][0])/Det;
		return Temp;
	}

	std::array<std::array<double, 4>, 4> Rows;
};

constexpr Matrix MatrixId = {{ {
	{{1, 0, 0, 0}},
	{{0, 1, 0, 0}},
	{{0, 0, 1, 0}},
	{{0, 0, 0, 1}} } }};
#endif
