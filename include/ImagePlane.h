#ifndef IMAGE_PLANE_H
#define IMAGE_PLANE_H

#include <vector>
#include <string>

#include "Vector.h"

struct ImagePlane {
	ImagePlane(int W, int H) : Width(W), Height(H), Pixels(Width * Height) {}
	
	int Width, Height;
	std::vector<Vector> Pixels;

	Vector& operator()(int x, int y);

	void writeToPPM(const std::string& Name = "out.ppm");

};

#endif
