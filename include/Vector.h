#ifndef VECTOR_H
#define VECTOR_H
#include <cmath>

struct Vector {
	const Vector operator+(const Vector &V) const {
		return {x + V.x, y + V.y, z + V.z};
	}

	const Vector operator+(double D) const {
		return {x + D, y + D, z + D};
	}

	const Vector operator-(const Vector &V) const {
		return {x - V.x, y - V.y, z - V.z};
	}

	const Vector operator*(double D) const {
		return {x *D, y * D, z * D};
	}

	const Vector operator*(const Vector &V) const {
		return {x * V.x, y * V.y, z * V.z};
	}

	double length() const {
		return std::sqrt(x*x + y*y + z*z);
	}

	const Vector unit() const {
		double len = length();
		return { x / len, y / len, z / len };
	}

	double dot(const Vector &V) const {
		return (x*V.x) + (y*V.y) + (z*V.z);
	}

	double x, y, z;
};

constexpr Vector UnitX { 1, 0, 0 };
constexpr Vector UnitY { 0, 1, 0 };
constexpr Vector UnitZ { 0, 0, 1 };
#endif
