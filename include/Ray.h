#ifndef RAY_H
#define RAY_H

#include "Vector.h"
#include "Matrix.h"

struct Ray {
	Vector Origin, Direction;

	Ray transform(const Matrix &M) const {
		Vector O = M * Origin;
		Vector D = (M*(Origin + Direction)) - O;
		return { O, D.unit() };
	}
};

#endif
